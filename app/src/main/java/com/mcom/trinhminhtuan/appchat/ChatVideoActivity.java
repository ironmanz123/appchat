package com.mcom.trinhminhtuan.appchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class ChatVideoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_video);
        WebView myWebView = (WebView) findViewById(R.id.webview);
        myWebView.loadUrl("https://smartglass01.herokuapp.com/");
    }
}
